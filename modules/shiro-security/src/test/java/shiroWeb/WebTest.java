package shiroWeb;
import org.junit.Ignore;
import org.junit.Test;

import com.gargoylesoftware.htmlunit.WebAssert;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

//@Ignore
public class WebTest  extends AbstractContainerTest{
	 @Test
     public void test() throws Exception{		   
		 HtmlPage page = webClient.getPage(getBaseUri() + "index.html");		
		 WebAssert.assertTextPresent(page, "hello");		
     }
}
