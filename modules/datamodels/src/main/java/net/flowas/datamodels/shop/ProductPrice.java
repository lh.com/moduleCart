/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flowas.datamodels.shop;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import lombok.Data;

/**
 *
 * @author liujh
 */
@Data
@Entity
public class ProductPrice extends IdAndDate{
    private Long retailerId;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date startDate;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date endDate;    
    @ManyToOne
    private Product product;
    @Transient
    protected BigDecimal salePrice;
    @Transient
    protected BigDecimal moneySaving;
    private BigDecimal catalogPrice;
}
